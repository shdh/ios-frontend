/*
File: global.js
Author: Shawn Dhawan
Description: Holds temporary testing variables
Date Last Modified: June 28, 2014
*/

/*These variables will be required to identify the course student is in, and their student id for identification purposes*/

var student_id = 1;
var class_id = 1;
/*
TO DO:

Make so when a class from class list is selected it will store the value in class_id.

Make so when a student logs in it will store their student id in variable student_id.

--Still not sure which group will handle this, maybe account management group--
*/